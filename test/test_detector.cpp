#include "grasp/grasp_finder.hpp"

#include <iostream>

#include <cvx/util/misc/path.hpp>
#include <cvx/util/camera/camera.hpp>
#include <Eigen/Core>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/filters/voxel_grid.h>

#include <pcl/io/pcd_io.h>


using namespace std;
using namespace cv ;
using namespace Eigen ;

using namespace cvx::util ;

typedef pcl::PointCloud<pcl::PointXYZ> CloudType ;

void depthToPointCloud(const cv::Mat &depth, const PinholeCamera &model_, pcl::PointCloud<pcl::PointXYZ> &cloud)
{

    cloud.width = depth.cols ;
    cloud.height = depth.rows ;
    cloud.is_dense = false ;
    cloud.points.resize(cloud.height * cloud.width) ;

    float center_x = model_.cx();
    float center_y = model_.cy();

    float bad_point = std::numeric_limits<float>::quiet_NaN();

    pcl::PointCloud<pcl::PointXYZ>::iterator pt_iter = cloud.begin();

    if ( depth.type() == CV_16UC1 )
    {
        double unit_scaling = 0.001 ;
        float constant_x = unit_scaling / model_.fx();
        float constant_y = unit_scaling / model_.fy();

        cv::Mat_<ushort> depth_(depth) ;

        for(int i=0 ; i<depth.rows ; i++)
            for(int j=0 ; j<depth.cols ; j++)
            {
                pcl::PointXYZ & pt = *pt_iter++;
                ushort val = depth_[i][j] ;

                if ( val == 0 ) {
                    pt.x = pt.y = pt.z = bad_point;
                    continue;
                }

                pt.x = (j - center_x) * val * constant_x;
                pt.y = (i - center_y) * val * constant_y;
                pt.z = val * unit_scaling ;
            }

    }
    else if ( depth.type() == CV_32FC1 )
    {
        float constant_x = 1.0 / model_.fx();
        float constant_y = 1.0 / model_.fy();

        cv::Mat_<float> depth_(depth) ;

        for(int i=0 ; i<depth.rows ; i++)
            for(int j=0 ; j<depth.cols ; j++)
            {
                pcl::PointXYZ & pt = *pt_iter++;
                float val = depth_[i][j] ;

                if ( std::isnan(val) ) {
                    pt.x = pt.y = pt.z = bad_point;
                    continue;
                }

                pt.x = (j - center_x) * val * constant_x;
                pt.y = (i - center_y) * val * constant_y;
                pt.z = val  ;
            }
    }

}

#define DEBUG

void detectAll(const CloudType &cloud, const Vector3d &up, vector<Vector4d> &planes)
{
    // resample cloud

    CloudType::Ptr cloud_filtered(new CloudType) ;
    pcl::PCLPointCloud2::Ptr cloud_blob (new pcl::PCLPointCloud2), cloud_filtered_blob (new pcl::PCLPointCloud2);

    pcl::toPCLPointCloud2(cloud, *cloud_blob) ;

    pcl::VoxelGrid<pcl::PCLPointCloud2> sor;
    sor.setInputCloud (cloud_blob);
    sor.setLeafSize (0.01, 0.01, 0.01);
    sor.filter (*cloud_filtered_blob);

    pcl::fromPCLPointCloud2 (*cloud_filtered_blob, *cloud_filtered);

    // find plane candidates with RANSAC

    pcl::SACSegmentation<pcl::PointXYZ> seg;
    seg.setOptimizeCoefficients (true);
    seg.setProbability(0.99);
    seg.setModelType (pcl::SACMODEL_PLANE);
//    seg.setAxis(up.cast<float>()) ;
//    seg.setEpsAngle(0.5) ;
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setDistanceThreshold (0.05);
    seg.setMaxIterations(1000);

    pcl::ExtractIndices<pcl::PointXYZ> extract;

    while (1)
    {
        pcl::PointIndicesPtr inliers_plane(new pcl::PointIndices) ;
        pcl::ModelCoefficients coeff ;

        // Add input cloud and segment
        seg.setInputCloud (cloud_filtered);
        seg.segment (*inliers_plane, coeff);

        if ( inliers_plane->indices.size() < 100 ) break ;

        Vector4d cand(coeff.values[0], coeff.values[1], coeff.values[2], coeff.values[3]) ;

        if ( cand.x() * up.x() + cand.y() * up.y() + cand.z() * up.z() < 0 )
            cand = -cand ;

        CloudType::Ptr cloud_f(new CloudType) ;
        extract.setInputCloud (cloud_filtered);
        extract.setIndices (inliers_plane);

#ifdef DEBUG
        CloudType cloud_i ;
        extract.setNegative (false);
        extract.filter (cloud_i);

        pcl::io::savePCDFile("/tmp/plane.pcd", cloud_i) ;
#endif
        extract.setNegative (true);
        extract.filter (*cloud_f);
        cloud_filtered.swap (cloud_f);

#ifdef DEBUG
         pcl::io::savePCDFile("/tmp/filtered.pcd", *cloud_filtered) ;
#endif
        planes.push_back(cand) ;

        break ;

    }

}

void detectPlane(const cv::Mat &depth, const PinholeCamera &cam, Eigen::Vector4d &p) {
    CloudType cloud ;
    depthToPointCloud(depth, cam, cloud);

    vector<Vector4d> planes ;
    detectAll(cloud, Vector3d(0, 0, -1), planes) ;

    p = planes[0] ;
}



int main(int argc, char **argv)
{
    cv::Mat clr, depth, mask ;

    clr = cv::imread("/tmp/pickup_rgbCloth.png") ;
    depth = cv::imread("/tmp/pickup_depthCloth.png", -1) ;
    mask = cv::Mat(clr.size(), CV_8UC1) ;
    mask = cv::Scalar(255) ;

    PinholeCamera cam(525, 525, 640/2.0, 480/2, cv::Size(640, 480)) ;

    Eigen::Vector4d plane ;

    detectPlane(depth, cam, plane) ;

    GraspFinder gfinder ;
    vector<GraspCandidate> gsp ;
    gfinder.find(clr, depth, mask, cam, plane, gsp) ;

    gfinder.draw(clr, gsp) ;
    cv::imwrite("/tmp/gsp.png", clr) ;

}


