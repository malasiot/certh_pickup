#include "grasp_finder.hpp"
#include "texture_segmentation.hpp"

#include <opencv2/opencv.hpp>

#include <cvx/util/geometry/line.hpp>
#include <cvx/util/misc/cv_helpers.hpp>
#include <cvx/util/geometry/trimesh_topology.hpp>
#include <cvx/util/geometry/polygon_scanner.hpp>
#include <cvx/util/geometry/polygon.hpp>
#include <cvx/util/imgproc/concomp.hpp>
#include <cvx/util/imgproc/rgbd.hpp>

#include <Eigen/SVD>
#include <Eigen/Eigenvalues>

#include <fstream>
#include <queue>

#include <opencv2/flann/flann.hpp>

using namespace std ;

using namespace Eigen ;
using namespace cvx::util ;

static void nonMaximaSuppressionDirectional(const cv::Mat& src, cv::Mat& dst, const cv::Mat &alpha_, const cv::Mat &sigma_)
{
    int w = src.cols, h = src.rows ;

    dst = cv::Mat(h, w, CV_8UC1) ;

    for( int i=0 ; i<h ; i++ )
        for( int j=0 ; j<w ; j++ )
        {
            double alpha = alpha_.at<float>(i, j) ;
            double sigma = sigma_.at<float>(i, j) ;

            double sa = cos(alpha) ;
            double ca = -sin(alpha) ;

            int ppx = j + sigma *  ca + 0.5;
            int ppy = i + sigma *  sa + 0.5 ;

            int pnx = j - sigma *  ca + 0.5;
            int pny = i - sigma *  sa + 0.5 ;

            vector<cv::Point> pts ;

            getScanLine(cv::Point(pnx, pny), cv::Point(ppx, ppy), pts) ;

            float maxV = 0.0 ;
            cv::Point mp ;

            for( int k=0 ; k<pts.size() ; k++ )
            {
                const cv::Point &p = pts[k] ;

                if ( p.x < 0 || p.y < 0 || p.x > src.cols - 1 || p.y > src.rows - 1 ) continue ;

                float v = src.at<float>(pts[k]) ;

                if ( v > maxV )
                {
                    maxV = v ;
                    mp = pts[k] ;
                }
             }

            if ( i == mp.y && j == mp.x ) dst.at<uchar>(i, j) = 255 ;
            else dst.at<uchar>(i, j) = 0 ;
        }
}

static void fitParabola(const vector<float> &X, const vector<float> &Y, double &a, double &b, double &c)
{
    int n = X.size() ;

    MatrixXf A(n, 3) ;
    VectorXf B(n) ;

    for( int i=0 ; i<n ; i++ )
    {
        A(i, 0) = X[i] * X[i] ;
        A(i, 1) = X[i] ;
        A(i, 2) = 1.0 ;
        B[i] = Y[i] ;
    }

    JacobiSVD<MatrixXf> svd(A, ComputeThinU | ComputeThinV) ;
    VectorXf sol = svd.solve(B) ;

    a = sol[0] ;
    b = sol[1] ;
    c = sol[2] ;

}

class Sorter {
public:
    bool operator () ( const std::pair<float, float> &c1, const std::pair<float, float> &c2 )
    {
        return c1.first < c2.first ;
    }
};

/*
@INPROCEEDINGS{Koller95,
    author={Koller, T.M. and Gerig, G. and Szekely, G. and Dettwiler, D.},
    booktitle={Computer Vision, 1995. Proceedings., Fifth International Conference on},
    title={Multiscale detection of curvilinear structures in 2-D and 3-D image data},
    year={1995},
    pages={864-869}
}
*/

double GraspFinder::graspability(const Vector2d &p0, double alpha, const cv::Mat &src, float &area)
{
    double ca = -cos(alpha) ;
    double sa = sin(alpha) ;

    Vector2d d(ca, -sa) ;
    Vector2d n(sa, ca) ;

    // find all points that are withing the gripper's profile (openned)

    Polygon2d poly(4) ;

    poly[0] = p0 + d * params_.gripper_width_/2.0 + n * params_.gripper_openning_/2.0 ;
    poly[1] = p0 + d * params_.gripper_width_/2.0 - n * params_.gripper_openning_/2.0 ;
    poly[2] = p0 - d * params_.gripper_width_/2.0 - n * params_.gripper_openning_/2.0 ;
    poly[3] = p0 - d * params_.gripper_width_/2.0 + n * params_.gripper_openning_/2.0 ;

    vector<Point2i> pts ;
    getPointsInPoly(poly.mat().cast<float>(), pts);

    // project points to the center line

    vector<pair<float, float> > X ;

    area = 0.0 ;

#ifdef _DEBUG_

    cv::Mat srcd = src_.clone() ;

    ofstream strm("/tmp/plot.txt") ;
#endif

    double zMax =0.0, zTop = 0.0 ;

    for(int i=0 ; i<pts.size() ; i++ )
    {
        const Point2i &p = pts[i] ;

        int x_ = p.x() ;
        int y_ = p.y() ;

#ifdef _DEBUG_
        srcd.at<uchar>(y_, x_) = 255 ;
#endif
        if ( x_<0 || y_<0 || x_ > src.cols-1 || y_ > src.rows-1 ) continue ;

        ushort val = src.at<ushort>(y_, x_) ;

        if ( val != 0 ) area ++ ;

        double ds = Point2d(x_ - p0.x(), y_ - p0.y()).dot(n) ;
        double qs = Point2d(x_ - p0.x(), y_ - p0.y()).dot(d) ;

        if ( fabs(ds) > 0.9 * params_.gripper_openning_/2.0 )
        {
            zMax = std::max(zMax, (double)val) ;
        }
        else {
            zTop = std::max(zTop, (double)val) ;
        }

        X.push_back(pair<float, float>(ds, val)) ;

#ifdef _DEBUG_
        strm << ds << ' ' << qs << ' ' << val << endl ;
#endif
    }

#ifdef _DEBUG
    strm.flush() ;
    strm.close() ;

    cv::imwrite("/tmp/gr.png", srcd) ;
#endif

    if ( X.empty() ) return 0 ;

    std::sort(X.begin(), X.end(), Sorter() ) ;

    // assume that the gripper will go down until it collides with one point (to the left or right of the ridge)

    // compute the volume contained in the gripper openning

    double volume = 0.0 ;

    for( int i=0 ; i<X.size() ; i++ )
    {
        double zThresh = std::max(zMax, zTop - params_.gripper_depth_ * 1000) ;

        if ( fabs(X[i].first) < 0.9 * params_.gripper_openning_/2.0 && X[i].second > zThresh )
            volume += X[i].second - zThresh ;
    }

    return volume ;

}

void GraspFinder::refineCandidate(GraspCandidate &cand, const cv::Mat &src)
{
    const double searchMin = -2.0 ;
    const double searchMax = 2.0 ;
    const double searchStep = 0.5 ;

    double ca = -cos(cand.alpha_) ;
    double sa = sin(cand.alpha_) ;

    Point2d d(ca, -sa) ;
    Point2d n(sa, ca) ;

    double best_offset = 0.0 ;
    double max_g = 0.0 ;
    double best_area = 0.0 ;

    for( double offset = searchMin ; offset <= searchMax ; offset += searchStep )
    {
        Point2d p(cand.x_, cand.y_) ;

        p += n * offset ;

        float area ;

        double g = graspability(p, cand.alpha_, src, area) ;

        if ( g > max_g )
        {
            max_g = g ;
            best_offset = offset ;
            best_area = area ;
        }
    }

    cand.volume_ = max_g ;

    cand.area_ = best_area ;

    Point2d pt(cand.x_, cand.y_) ;
    pt +=  n * best_offset ;

    cand.x_ = pt.x() ;
    cand.y_ = pt.y() ;

}

cv::Mat GraspFinder::detect(const cv::Mat &src, cv::Mat &alpha, cv::Mat &scale, cv::Mat &ridges)
{
    cv::Mat gray;
    if (src.type() == CV_8UC1)
    {
        src.convertTo(gray, CV_8UC1);
        cout << "rgb image" << endl;
    }
    else
        src.convertTo(gray, CV_32FC1) ;
/*
    for(int i=0 ; i<gray.rows ; i++ )
        for(int j=0 ; j<gray.cols ; j++ )
            if ( gray.at<float>(i, j) == 0.0 ) gray.at<float>(i, j) = 1255 ;
*/
    src.convertTo(src_, CV_8UC1) ;

    int w = src.cols, h = src.rows ;

    double response_threshold = params_.response_threshold_ ;

    cv::Mat resp(h, w, CV_32FC1, cv::Scalar(0)) ;
    alpha = cv::Mat(h, w, CV_32FC1, cv::Scalar(0)) ;
    scale = cv::Mat(h, w, CV_32FC1, cv::Scalar(0)) ;

    for( double sigma = params_.min_scale_ ; sigma <= params_.max_scale_ ; sigma += params_.scale_step_ )
    {
        cv::Mat blured ;

        cv::GaussianBlur(gray, blured, cv::Size(0, 0), sigma) ;

        cv::Mat gxy, g2x, g2y, gx, gy;

        cv::Sobel(blured, gxy, CV_32FC1, 1, 1) ;
        cv::Sobel(blured, g2x, CV_32FC1, 2, 0) ;
        cv::Sobel(blured, g2y, CV_32FC1, 0, 2) ;

        cv::Sobel(blured, gx, CV_32FC1, 1, 0) ;
        cv::Sobel(blured, gy, CV_32FC1, 0, 1) ;

        for( int i=0 ; i<h ; i++ )
            for( int j=0 ; j<w ; j++ )
            {
                // find the orinetation of the ridge

                float gxy_ = gxy.at<float>(i, j) ;
                float g2x_ = g2x.at<float>(i, j) ;
                float g2y_ = g2y.at<float>(i, j) ;
                float gx_ = gx.at<float>(i, j) ;
                float gy_ = gy.at<float>(i, j) ;

                float gd = g2x_ - g2y_ ;

                if ( gx_ * gx_ + gy_ * gy_ < 5 * 5 ) continue ;

                double a = atan2(2*gxy_, gd)/2 ;

                double sa = cos(a) ;
                double ca = -sin(a) ;

                int ppx = j + sigma *  ca + 0.5;
                int ppy = i + sigma *  sa + 0.5 ;

                int pnx = j - sigma *  ca + 0.5;
                int pny = i - sigma *  sa + 0.5 ;

                if ( ppx < 0 || ppy < 0 || ppx > w-1 || ppy > h-1 ) continue ;
                if ( pnx < 0 || pny < 0 || pnx > w-1 || pny > h-1 ) continue ;

                double rl = gx.at<float>(ppy, ppx) * ca + gy.at<float>(ppy, ppx) * sa ;
                double rr = gx.at<float>(pny, pnx) * ca + gy.at<float>(pny, pnx) * sa ;

                rl = std::max(-rl, 0.0) ;
                rr = std::max(rr, 0.0) ;

                double response = std::min(rl, rr) ;

                if ( response < response_threshold ) continue ;

                if ( response > resp.at<float>(i, j) )
                {
                    resp.at<float>(i, j) = response ;
                    alpha.at<float>(i, j) = a ;
                    scale.at<float>(i, j) = sigma ;
                }

            }

    }

    nonMaximaSuppressionDirectional(resp, ridges, alpha, scale);

    return resp ;
}

struct CandSorterStrength {
    bool operator() (const GraspCandidate &c1, const GraspCandidate &c2)
    {
        return c1.strength_ >= c2.strength_ ;

    }
};



// prune candidate features and sort them by strength
void GraspFinder::findCandidates(const cv::Mat &src, const cv::Mat &resp, const cv::Mat &ridges, const cv::Mat &alpha, const cv::Mat &scale, std::vector<GraspCandidate> &cand_)
{
    int w = src.cols, h = src.rows ;

    vector<GraspCandidate> cand, candList ;

    for( int i=0 ; i<h ; i++ )
        for(int j=0 ; j<w ; j++ )
        {
            if ( ridges.at<uchar>(i, j) == 0 ) continue ;

            GraspCandidate grp ;
            grp.x_ = j ;
            grp.y_ = i ;
            grp.strength_ = resp.at<float>(i, j) ;
            grp.width_ = scale.at<float>(i, j) ;
            grp.alpha_ = alpha.at<float>(i, j) ;
            grp.volume_ = 0.0 ;

            cand.push_back(grp) ;
        }

    sort(cand.begin(), cand.end(), CandSorterStrength()) ;

    cv::Mat_<float> flann_index_data(cand.size(), 2);

    for(int i=0 ; i<cand.size() ; i++ )
    {
        flann_index_data.at<float>(i, 0) = cand[i].x_ ;
        flann_index_data.at<float>(i, 1) = cand[i].y_ ;
    }

    cv::flann::Index *spatial_index =  new cv::flann::Index(flann_index_data,
           cv::flann::KDTreeIndexParams(4),
           cvflann::FLANN_DIST_EUCLIDEAN
    );


    // remove neighbors of the best candidate that have similar orientations

    const double radius = 16 ;

    vector<bool> checked ;
    checked.resize(cand.size(), false) ;

    int c = 0 ;


    while ( 1 )
    {
        while ( c<checked.size() && checked[c] ) ++c ;
        if ( c == checked.size() - 1 ) break ;

        // insert best candidate in the list

        GraspCandidate best = cand[c] ;

        candList.push_back(best) ;
        checked[c] = true ;

        double sa = -sin(best.alpha_) ;
        double ca = cos(best.alpha_) ;
        Vector2d d(sa, ca) ;

        ++c ;

        int j = c ;

        cv::Mat_<float> query(1, 2);

        query(0, 0) = best.x_ ;
        query(0, 1) = best.y_ ;

        cv::Mat_<int> idxs ;
        cv::Mat_<float> dists ;

        int nRes = spatial_index->radiusSearch(query, idxs, dists, radius*radius, cand.size(), cv::flann::SearchParams(32)) ;

        for(int i=0 ; i<nRes ; i++ )
            checked[idxs[0][i]] = true ;
    }

    cout << "candList size = " << candList.size() << endl;
    for(int i=0 ; i<candList.size() ; i++ )
    {
        GraspCandidate &cnd = candList[i] ;
        refineCandidate(cnd, src) ;

        if (src.type() == CV_8UC1)
        {
//            cout << "Area checking" << endl;
            if ( cnd.area_ < 5 ) continue ;
        }
        else
        {
            cout << "Volume checking" << endl;
            if ( cnd.volume_ < params_.min_grasp_volume_ ) continue ;
        }

        cand_.emplace_back(cnd) ;
    }


    delete spatial_index ;
}


cv::Mat GraspFinder::fillEdgeImage(const cv::Mat &edgesIn)
{
    cv::Mat edgesNeg = edgesIn.clone() ;
    cv::Mat filledEdgesOut ;

    cv::floodFill(edgesNeg, cv::Point(0,0), CV_RGB(255,255,255)) ;
    bitwise_not(edgesNeg, edgesNeg) ;
    filledEdgesOut = (edgesNeg | edgesIn) ;

    return filledEdgesOut ;
}

struct BlobData {

    BlobData(int label): label_(label) {}

    int label_ ;
    uint area_ = 0 ;
    uint max_depth_ = 0 ;
    cv::Mat dist_ ;
};

void GraspFinder::refineCandidates( const cv::Mat &depth, const cv::Mat &labels, std::vector<GraspCandidate> &cand) {

    std::vector<GraspCandidate> cand_out ;
    std::map<int, BlobData> blobs ;

    // collect supporting region of
    for ( int k=0; k<cand.size(); k++ )
    {
        // find corresponding blob for gsps and remove any candidate in spurious regions

        GraspCandidate &c = cand[k] ;
        int label = labels.at<int>(c.y_, c.x_) ;
        if ( label == INT_MAX ) continue ;

        c.blob_ = label ;
        BlobData bdata(label) ;
        blobs.insert(std::make_pair(label, bdata)) ;
        cand_out.emplace_back(c) ;
    }


    // process blobs

    RegionIterator it(labels) ;

    while ( it ) {
        int label = it.label() ;
        auto blobit = blobs.find(label) ;
        if ( blobit != blobs.end() ) {
            BlobData &bdata = blobit->second ;
            bdata.area_ = it.area() ;
            bdata.max_depth_ = 0 ;

            // compute minimum depth of blob

            cv::Rect rect = it.rect() ;

            for( uint y = rect.y ; y < rect.y + rect.height ; y++ ) {
                for( uint x = rect.x ; x < rect.x + rect.width ; x++ ) {
                    int blabel = labels.at<int>(y, x) ;
                    if ( blabel == label ) {
                        ushort z = depth.at<ushort>(y, x) ;
                        bdata.max_depth_ = std::max<ushort>(z, bdata.max_depth_) ;
                    }
                }
            }

            // compute distance transform

            cv::Mat fmask  = fillEdgeImage(it.mask());
            cv::distanceTransform(fmask, bdata.dist_, CV_DIST_L2, CV_DIST_MASK_PRECISE) ;
        }

        ++it ;
    }

    cand.clear() ;

    // now filter candidates close to the border and with small area

    for ( auto &c : cand_out )
    {
        // find corresponding blob for gsps

        int label = labels.at<int>(c.y_, c.x_) ;

        auto blobit = blobs.find(label) ;

        if ( blobit == blobs.end() ) continue ;

        BlobData &bdata = blobit->second ;

        // check area threshold

        if ( bdata.area_ < params_.min_blob_area_ ) continue ;

        // check if the candidate is close to region boundary

        float dist = bdata.dist_.at<float>(c.y_, c.x_) ;

        if ( dist < params_.min_region_boundary_dist_ ) continue ;

        // store minimum depth to use for sorting

        c.max_blob_depth_ = bdata.max_depth_ ;

        // insert candidate into final list

        cand.emplace_back(c) ;
    }

    // sort candidates so that we examine those of the highest blob first and with the
    // large volume within the blob

    std::sort(cand.begin(), cand.end(),
            [&](const GraspCandidate &c1, const GraspCandidate &c2) {

                if ( c1.blob_ != c2.blob_ )
                    return c1.max_blob_depth_ >= c2.max_blob_depth_ ;
                else
                    return c1.volume_ >= c2.volume_ ;
            }
    ) ;
}

cv::Mat GraspFinder::findObjectMask(const PinholeCamera &cam, const cv::Mat &depth, const cv::Mat &src_mask, const Eigen::Vector4d &plane, double dist_thresh, uint small_area_threshold, cv::Mat &dmap) {
    int w = depth.cols, h = depth.rows ;

    cv::Mat_<uchar> mask = cv::Mat_<uchar>::zeros(h, w) ;
    cv::Mat_<ushort> dmap_ = cv::Mat_<ushort>::zeros(h, w) ;
    cv::Mat_<ushort> depth_(depth) ;
    cv::Mat_<uchar> smask_(src_mask) ;

    for( int i=0 ; i<h ; i++ )
        for(int j=0 ; j<w ; j++ )
        {
            if ( smask_[i][j] == 0 ) continue ;

            ushort val = depth_[i][j] ;
            if ( val == 0 ) continue ;

            float z = val/1000.0 ;
            cv::Point3d cp = cam.backProject(cv::Point2d(j, i)) ;
            cp *= z ;

            Eigen::Vector4d p(cp.x, cp.y, cp.z, 1) ;

            double dist = p.dot(plane) ;

            if ( dist > dist_thresh ) {
                mask[i][j] = 255 ;
                dmap_[i][j] = dist * 1000 ;
            }
        }

    cv::Mat_<uchar> rmask = cv::Mat_<uchar>::zeros(h, w) ;

    // filter out small regions

    cv::Mat labels ;
    connectedComponents(mask, labels) ;

    RegionIterator it(labels) ;

    while ( it ) {
        if ( it.area() > small_area_threshold ) {
            cv::Rect rect = it.rect() ;
            unsigned long label = it.label() ;
            for( uint y = rect.y ; y<rect.y + rect.height ; ++y )
                for( uint x = rect.x ; x<rect.x + rect.width ; ++x )
                    if ( labels.at<unsigned int>(y, x) == label ) rmask[y][x] = 255;
        }
        ++it ;
    }

    dmap_.copyTo(dmap, rmask) ;

    return rmask ;

}

bool GraspFinder::find(const cv::Mat &clr, const cv::Mat &depth, const cv::Mat &src_mask, const PinholeCamera &cam, const Vector4d &plane, std::vector<GraspCandidate> &cand)
{
    cv::Mat dmap, labels ;
    cv::Mat mask = findObjectMask(cam, depth, src_mask, plane, params_.dist_above_plane_, params_.plane_segment_cluster_, dmap) ;

    TextureSegmentation tseg ;
    tseg.segment(clr, mask, labels) ;

    cv::imwrite("/tmp/omask.png", mask) ;
    cv::imwrite("/tmp/omap.png", depthViz(dmap)) ;

    GraspFinder gfinder ;
    vector<GraspCandidate> gsp ;

    gfinder.detect(dmap, gsp) ;

    gfinder.refineCandidates(dmap, labels, gsp);

    cand = gsp;

    return !gsp.empty() ;

}

bool GraspFinder::detect(const cv::Mat &src, std::vector<GraspCandidate> &cand)
{
    cv::Mat scale, alpha, ridges, resp ;

    resp = detect(src, alpha, scale, ridges) ;
    findCandidates(src, resp, ridges, alpha, scale, cand) ;

    return !cand.empty() ;

}


void GraspFinder::draw(cv::Mat &clr, const std::vector<GraspCandidate> &cand)
{
    std::map<int, cv::Vec3b> cmap ;

    for(int i=0 ; i<cand.size() ; i++ )
    {
        int b = cand[i].blob_ ;

        if ( cmap.count(b) == 0 )
            cmap[b] = cv::Vec3b(rand()%256, rand()%256, rand()%256) ;
    }

    for(int i=0 ; i<cand.size() ; i++ )
    {
        const GraspCandidate &cnd = cand[i] ;

        double sa = -sin(cnd.alpha_) ;
        double ca = cos(cnd.alpha_) ;

        Point2d d(sa, ca) ;

        Point2d p1 =  d * cnd.width_ + Point2d(cnd.x_, cnd.y_) ;
        Point2d p2 = -d * cnd.width_ + Point2d(cnd.x_, cnd.y_) ;

        cv::Vec3b c = cmap[cnd.blob_] ;

        if ( i==0 )
            cv::line(clr, cv::Point(p1.x(), p1.y()), cv::Point(p2.x(), p2.y()), cv::Scalar(0, 255, 0), 4 )  ;
        else
            cv::line(clr, cv::Point(p1.x(), p1.y()), cv::Point(p2.x(), p2.y()), cv::Scalar(c[0], c[1], c[2]), 1 )  ;

    }
}

bool GraspFinder::findObjectHull(const PinholeCamera &cam, const cv::Mat &depth, const cv::Mat &mask, const Vector4d &plane,
                                 std::vector<cv::Point> &hull, cv::Mat &dmap)
{
    int w = depth.cols, h = depth.rows ;

    cv::Mat_<uchar> omask = cv::Mat_<uchar>::zeros(h, w) ;
    cv::Mat_<ushort> depth_(depth) ;
    cv::Mat_<uchar> src_mask(mask) ;
    cv::Mat_<ushort> dmap_ = cv::Mat_<ushort>::zeros(h, w) ;

    for( int i=0 ; i<h ; i++ )
        for(int j=0 ; j<w ; j++ )
        {
            if ( src_mask[i][j] == 0 ) continue ;

            ushort val = depth_[i][j] ;
            if ( val == 0 ) continue ;

            float z = val/1000.0 ;
            cv::Point3d cp = cam.backProject(cv::Point2d(j, i)) ;
            cp *= z ;

            Eigen::Vector4d p(cp.x, cp.y, cp.z, 1) ;

            double dist = p.dot(plane) ;

            if ( dist > params_.dist_above_plane_ ) {
                omask[i][j] = 255 ;
                dmap_[i][j] = 1000*dist ;
            }

        }


    RegionIterator it = findLargestBlob(omask, params_.plane_segment_cluster_) ;

    if ( !it ) return false ;

    dmap_.copyTo(dmap, it.mask()) ;

    vector<cv::Point> contour = it.contour() ;

    cv::convexHull( cv::Mat(contour), hull, false);

    return true ;

}

