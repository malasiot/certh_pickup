#include "lbph.hpp"

#include <cvx/util/imgproc/gabor.hpp>

using namespace std ;
using namespace Eigen ;
using namespace cvx::util ;

static void lbp(const cv::Mat &src, cv::Mat &dst) {

    // // Ahonen et. al uniform patterns
    static int lbp_mapping[256] =  {0, 1, 2, 3, 4, 58, 5, 6, 7, 58, 58, 58, 8, 58, 9, 10, 11, 58, 58, 58, 58, 58, 58, 58,
                                    12, 58, 58, 58, 13, 58, 14, 15, 16, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58,
                                    58, 58, 17, 58, 58, 58, 58, 58, 58, 58, 18, 58, 58, 58, 19, 58, 20, 21, 22, 58, 58, 58,
                                    58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58,
                                    58, 58, 58, 58, 58, 58, 58, 58, 23, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58,
                                    58, 58, 24, 58, 58, 58, 58, 58, 58, 58, 25, 58, 58, 58, 26, 58, 27, 28, 29, 30, 58, 31,
                                    58, 58, 58, 32, 58, 58, 58, 58, 58, 58, 58, 33, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58,
                                    58, 58, 58, 58, 58, 34, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58,
                                    58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 35, 36, 37, 58, 38,
                                    58, 58, 58, 39, 58, 58, 58, 58, 58, 58, 58, 40, 58, 58, 58, 58, 58, 58, 58, 58, 58,
                                    58, 58, 58, 58, 58, 58, 41, 42, 43, 58, 44, 58, 58, 58, 45, 58, 58, 58, 58, 58, 58, 58, 46,
                                    47, 48, 58, 49, 58, 58, 58, 50, 51, 52, 58, 53, 54, 55, 56, 57};

    for(int i = 1; i<src.rows-1; i++)
        for(int j = 1; j< src.cols-1; j++)
        {
            int center = src.at<uchar>(i,j) ;

            unsigned char code = 0;

            code |= (src.at<uchar>(i-1,j-1) > center) << 7;
            code |= (src.at<uchar>(i-1,j) > center) << 6;
            code |= (src.at<uchar>(i-1,j+1) > center) << 5;
            code |= (src.at<uchar>(i,j+1) > center) << 4;
            code |= (src.at<uchar>(i+1,j+1) > center) << 3;
            code |= (src.at<uchar>(i+1,j) > center) << 2;
            code |= (src.at<uchar>(i+1,j-1) > center) << 1;
            code |= (src.at<uchar>(i,j-1) > center) << 0;

            dst.at<uchar>(i,j) = lbp_mapping[code];
         }
}



Eigen::VectorXf LBPH::compute(const cv::Mat &gray, const vector<cv::Rect> &tiles) {

    VectorXf f(tiles.size() * 58) ;

    uint idx = 0 ;
    cv::Mat_<uchar> dst = cv::Mat_<uchar>::zeros(gray.size()) ;
    dst = 58 ;

    lbp(gray, dst) ;

    for(uint j = 0 ; j<tiles.size() ; j++) {
        const cv::Rect &rect = tiles[j] ;

         uint hist[58] = {0}, count = 0 ;

         for(uint x=rect.x ; x < rect.x + rect.width ; x++)
             for(uint y=rect.y ; y < rect.y + rect.height ; y++)
             {
                 if ( x < 0 || y < 0 || x > gray.cols - 1 || y > gray.rows - 1 ) continue ;
                 uchar c = dst[y][x] ;
                 if ( c == 58 ) continue ;
                 hist[c] ++ ;
                 count ++ ;
             }

          for(uint k=0 ; k<58 ; k++ )
            f[idx++] = hist[k]/(float)count ;
    }

    return f ;
}

