#include "texture_segmentation.hpp"
#include "lbph.hpp"
#include "segment_graph.hpp"
#include "color_histogram.hpp"

#include <cvx/util/math/rng.hpp>

using namespace std ;
using namespace Eigen ;
using namespace cvx::util ;

static float distChiSquare(const VectorXf &h, uint idx1, uint idx2, uint n) {
    float sum = 0 ;
    for( uint i=0 ; i<n ; i++) {
        float a = (h[idx1+i] - h[idx2+i]) ;
        float b = (h[idx1+i] + h[idx2+i]) ;
        if ( b > 1.0e-10 )
              sum += a*a/b ;
    }

    return sum/n ;
}

static float diff(const VectorXf &desc, uint nbins, uint sw, uint x1, uint y1, uint x2, uint y2) {

    uint idx1 = nbins * (y1 * sw + x1) ;
    uint idx2 = nbins * (y2 * sw + x2) ;
    return distChiSquare(desc, idx1, idx2, nbins) ;
}

void TextureSegmentation::segment(const cv::Mat &src, const cv::Mat &mask, cv::Mat &olabels)
{
    assert(src.channels() == 3) ;

    cv::Mat blurred, gray ;
    cv::GaussianBlur(src, blurred, cv::Size(0, 0), 1.0 ) ;

    cv::cvtColor(blurred, gray, CV_RGB2GRAY);

    // compute LBP histogram of grayscale image

    const uint w = src.cols, h = src.rows ;
    vector<cv::Rect> blocks ;

    const cv::Size block_size(9, 9) ;
    const uint step = 4 ;
    const uint bsw = block_size.width/2, bsh = block_size.height/2 ;

    uint brows = 0, bcols ;
    for( uint i = bsh ; i < h - bsh ; i += step, brows++ ) {
        bcols = 0;
        for( uint j = bsw ; j < w - bsw ; j += step, bcols++ ) {
            blocks.push_back(cv::Rect(j - bsw, i - bsh, block_size.width, block_size.height)) ;
        }
    }

    LBPH lbph ;

    uint ntbins = lbph.bins() ;

    VectorXf tdesc = lbph.compute(gray, blocks) ;

    ColorHistogram chist ;

    VectorXf cdesc = chist.compute(blurred, blocks) ;

    uint ncbins = chist.bins() ;

    const float c = 0.1;
    const int min_size = 20 ;
    const float tweight = 0.5 ;
    // build edge graph to compute segmentation

    edge *edges = new edge [brows * bcols * 4] ;
    int num = 0 ;

    for ( uint i = 0; i < brows ; i++ ) {
        for ( uint j = 0 ; j < bcols ; j++  ) {
            if ( j < bcols-1 ) {
                edges[num].a = i *  bcols + j ;
                edges[num].b = i *  bcols + (j+1) ;
                edges[num].w = tweight * diff(tdesc, ntbins,  bcols, j, i, j+1, i) +
                        diff(cdesc, ncbins, bcols, j, i, j+1, i);
                num++ ;
            }

            if ( i < brows-1 ) {
                edges[num].a = i * bcols + j ;
                edges[num].b = (i+1) * bcols + j ;
                edges[num].w = tweight * diff(tdesc, ntbins,  bcols, j, i, j, i+1 ) +
                        diff(cdesc, ncbins, bcols, j, i, j, i+1);
                num++ ;
            }

            if ( ( i < brows-1 ) && ( j < bcols-1) ) {
                edges[num].a = i * bcols + j ;
                edges[num].b = (i+1) * bcols + (j+1) ;
                edges[num].w = tweight * diff(tdesc, ntbins, bcols, j, i, j+1, i+1 ) +
                        diff(cdesc, ncbins, bcols, j, i, j+1, i+1);
                num++ ;
            }

            if ( ( j < bcols-1) && (i > 0)) {
                edges[num].a = i * bcols + j ;
                edges[num].b = (i-1) * bcols + (j+1) ;
                edges[num].w = tweight * diff(tdesc, ntbins, bcols, j, i, j+1, i-1 ) +
                        diff(cdesc, ncbins, bcols, j, i, j+1, i-1);
                num++ ;
            }
        }
    }



    // segment
    universe *u = segment_graph(brows * bcols, num, edges, c) ;

    // post process small components
    for (int i = 0; i < num; i++) {
        int a = u->find(edges[i].a) ;
        int b = u->find(edges[i].b) ;
        if ((a != b) && ((u->size(a) < min_size) || (u->size(b) < min_size)))
            u->join(a, b) ;
    }
    delete [] edges ;

    cv::Mat labels = cv::Mat(brows, bcols, CV_32SC1) ;
    labels = cv::Scalar(INT_MAX) ;

    for ( uint i = 0; i < brows ; i++ ) {
        for ( uint j = 0 ; j < bcols ; j++  ) {

            int comp = u->find(i * bcols + j);
            labels.at<int>(i, j) = comp ;
        }
    }

    // resize label map to source image dimensions

    cv::Mat rlabels ;
    cv::Mat flabels(src.size(), CV_32SC1) ;
    cv::Mat masked(src.size(), CV_32SC1) ;
    flabels = cv::Scalar(INT_MAX) ;
    masked = cv::Scalar(INT_MAX) ;

    cv::resize(labels, rlabels, cv::Size(bcols * step, brows * step), 0, 0, cv::INTER_NEAREST) ;

    rlabels.copyTo(flabels(cv::Rect(bsw, bsh, rlabels.cols, rlabels.rows))) ;
    flabels.copyTo(masked, mask) ;

//#ifdef DEBUG
    cv::Mat clabels = cv::Mat(src.size(), CV_8UC3) ;

    // pick random colors for each component
    std::vector< cv::Vec3b > colors ;

    RNG rng ;

    for (int i = 0; i < src.cols * src.rows ; i++)  {
        uint r = rng.uniform<uint>(0, 255) ;
        uint g = rng.uniform<uint>(0, 255) ;
        uint b = rng.uniform<uint>(0, 255) ;
        colors.push_back(cv::Vec3b(b, g, r)) ;
    }

    for ( uint i = 0; i < src.rows ; i++ ) {
        for ( uint j = 0 ; j < src.cols ; j++  ) {
            int val = masked.at<int>(i, j) ;
            if ( val == INT_MAX )
                clabels.at<cv::Vec3b>(i, j) = cv::Vec3b(0, 0, 0) ;
            else
                clabels.at<cv::Vec3b>(i, j) = colors[val];
        }
    }

    cv::imwrite("/tmp/clabels.png", clabels) ;
//#endif
    olabels = masked ;
}
