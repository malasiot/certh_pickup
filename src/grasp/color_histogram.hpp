#ifndef __COLOR_HISTOGRAM_HPP__
#define __COLOR_HISTOGRAM_HPP__

#include <Eigen/Core>
#include <opencv2/opencv.hpp>

// Color histogram on RGB color space
//

class ColorHistogram {
public:


    ColorHistogram() {}

    uint bins() const { return 27 ; }

    Eigen::VectorXf compute(const cv::Mat &im, const std::vector<cv::Rect> &tiles) ;

private:
    void makeColorHistogram(const std::vector<cv::Vec3b> &rgb, cv::Mat &hist);
} ;




#endif
