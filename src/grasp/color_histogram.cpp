#include "color_histogram.hpp"

using namespace std ;
using namespace Eigen ;

void ColorHistogram::makeColorHistogram(const std::vector<cv::Vec3b> &rgb, cv::Mat &hist )
{
    hist = cv::Mat::zeros(27, 1, CV_32FC1) ;

    for(int i=0 ; i<rgb.size() ; i++)
    {
        // color conversion
        cv::Mat_<cv::Vec3b> rgbp(1, 1), hsvp ;
        rgbp = cv::Vec3b(rgb[i][0], rgb[i][1], rgb[i][2]) ;
        cv::cvtColor(rgbp, hsvp, cv::COLOR_RGB2HSV) ;
        cv::Vec3b hsv = hsvp[0][0] ;

        if ( hsv[1] < 20 )
            hist.at<float>(25, 0) ++ ;
        else if ( hsv[1] > 235 )
            hist.at<float>(26, 0) ++ ;
        else
        {
            float h = hsv[0]*25/180.0 ;
            int b1 = (int)h, b2 = b1 + 1 ;
            float w1 = h - b1, w2 = b2 - h ;
            hist.at<float>(b1, 0) += w2 ;
            if ( b2 < 25 ) hist.at<float>(b2, 0) += w1 ;
            else hist.at<float>(0, 0) += w1 ;
        }
    }

    hist /= rgb.size() ;
}

VectorXf ColorHistogram::compute(const cv::Mat &rgb, const std::vector<cv::Rect> &tiles) {
    VectorXf out(tiles.size() * bins()) ;
    uint idx = 0 ;
    for( uint i=0 ; i<tiles.size() ; i++ ) {
        vector<cv::Vec3b> clrs ;
        cv::Mat hist ;

        const cv::Rect &rect = tiles[i] ;
        for(int y=rect.y ; y <rect.y + rect.height ; y++)
            for(int x=rect.x ; x <rect.x + rect.width ; x++)
            {
               const cv::Vec3b &bgr = rgb.at<cv::Vec3b>(y, x) ;
               clrs.push_back(cv::Vec3b(bgr[2], bgr[1], bgr[0])) ;
           }

        makeColorHistogram(clrs, hist);

        for( uint k=0 ; k<bins() ; k++ ) {
            out[idx++] = hist.at<float>(k, 0) ;
        }
    }

    return out ;
}
