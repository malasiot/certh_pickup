#ifndef __TEXTURE_SEGMENTATION_HPP__
#define __TEXTURE_SEGMENTATION_HPP__

#include <opencv2/opencv.hpp>

class TextureSegmentation {
public:
    struct Parameters {

    };

    TextureSegmentation(const Parameters &params): params_(params) {}
    TextureSegmentation() = default ;

    void segment(const cv::Mat &src, const cv::Mat &mask, cv::Mat &labels) ;

private:
    Parameters params_ ;

};















#endif
