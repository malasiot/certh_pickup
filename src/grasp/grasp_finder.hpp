#ifndef __RIDGE_DETECTOR_HPP__
#define __RIDGE_DETECTOR_HPP__

#include <opencv2/opencv.hpp>
#include <cvx/util/geometry/point.hpp>
#include <cvx/util/camera/camera.hpp>

struct GraspCandidate {
      double x_, y_ ; // position on the image
      double width_ ;   // width of the ridge
      double alpha_ ;   // angle
      double strength_ ; // strength of the ridge
      double volume_ ; // volume of surface in the gripper openning
      double area_ ; // non-zero pixel area
      float distance_ ; //distrance tranform value
      int blob_ ; //corresponding blob
      double max_blob_depth_ ;

  };

class GraspFinder {
public:

    struct Parameters {
        double response_threshold_ = 5.0 ; // ridge detector response threshold
        double min_scale_ = 4.0, max_scale_ = 8.0, scale_step_ = 0.25 ; // for multi-scale search
        double gripper_openning_ = 20 ; // approximate openning of the gripper in pixels
        double gripper_width_ = 12 ;    // approximate width of the gripper in pixels
        double gripper_depth_ = 0.025 ;    // approximate sinking of the gripper with respect to the candidate point (m)

        double min_blob_area_ = 4000 ;     // minimum area of blob to consider
        double min_region_boundary_dist_ = 10 ; // minimum distance of a candidate to region boundary
        double min_grasp_volume_ = 100 ; // threshold for graspability of candidate
        double dist_above_plane_ = 0.05 ; // consider points above supporting plane that are this far
        double plane_segment_cluster_ = 100 ; // small clusters of pixels after plane segmentation
    };

    GraspFinder() {}
    GraspFinder(const Parameters &params): params_(params) {}

    // main entry point:
    // segment all point above the plane and run the detection algorithm to obtained a list of ranked grasp candidates

    bool find(const cv::Mat &rgb, const cv::Mat &depth, const cv::Mat &mask, const cvx::util::PinholeCamera &cam,
              const Eigen::Vector4d &plane, std::vector<GraspCandidate> &cand) ;

    // run detector to obtain a list of grasp candidates sorted by strength

    bool detect(const cv::Mat &src, std::vector<GraspCandidate> &cand) ;

    //fill holes in mask

    cv::Mat fillEdgeImage(const cv::Mat &edgesIn) ;

    // refine candidates by supporting region

    void refineCandidates( const cv::Mat &depth, const cv::Mat &labels, std::vector<GraspCandidate> &cand) ;

    // draw candidates on a color image

    void draw(cv::Mat &clr, const std::vector<GraspCandidate> &cand) ;

    // segment objects above plane and find the hull of the largest blob

    bool findObjectHull(const cvx::util::PinholeCamera &cam, const cv::Mat &depth, const cv::Mat &mask, const Eigen::Vector4d &plane,
                        std::vector<cv::Point> &hull, cv::Mat &dmap);

protected:

    // detect ridges on the depth map (Koller et. al, Multiscale Detection of Curvilinear Structures in 2D and 3D Image data)
    // The result is a binary map designating ridge points
    // The images alpha and sigma are the principal curvature orientation of the ridge and sigma is proportional ro the width of the ridge

    cv::Mat detect(const cv::Mat &src, cv::Mat &alpha, cv::Mat &sigma, cv::Mat &ridges) ;

    // select candidates by a non-maximum supression procedure for features with similar orientation

    void findCandidates(const cv::Mat &src, const cv::Mat &resp, const cv::Mat &ridges,
                                const cv::Mat &alpha, const cv::Mat &sigma, std::vector<GraspCandidate> &cand) ;

    // compute strength of a detected ridge point with a gripper model

    void refineCandidate(GraspCandidate &cand, const cv::Mat &src) ;

    double graspability(const Eigen::Vector2d &p0, double alpha, const cv::Mat &src, float &area) ;

    cv::Mat findObjectMask(const cvx::util::PinholeCamera &cam, const cv::Mat &depth, const cv::Mat &mask, const Eigen::Vector4d &plane, double dist_thresh, uint small_area_threshold, cv::Mat &dmap);

private:

    Parameters params_ ;
    cv::Mat src_ ;
};


#endif
