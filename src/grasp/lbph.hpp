#ifndef __LGBPS_HPP__
#define __LGBPS_HPP__

#include <Eigen/Core>
#include <opencv2/opencv.hpp>

// Local binary pattern histograms over an image
//

class LBPH {
public:


    LBPH() {}

    uint bins() const { return 58 ; }

    // compute LGBP histogram over image tiles i.e. filters the image with the Gabor filterbank and then for each tile we compute LBP
    // histogram on all the Gabor channels
    Eigen::VectorXf compute(const cv::Mat &im, const std::vector<cv::Rect> &tiles) ;

} ;




#endif
