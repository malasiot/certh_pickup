#include "ros/ros.h"
#include "PickUp.h"
#include <robot_helpers/Robot2.h>
#include <robot_helpers/Utils2.h>

#include <camera_helpers/OpenNIServiceClient.h>




using namespace robot_helpers ;

int main(int argc, char **argv) {



    ros::init(argc, argv, "pick_cloth_from_table");
    ros::NodeHandle nh;
    system("rosrun camera_helpers openni_service xtion2 &") ;
    ros::Duration(3).sleep() ;

    Robot2 rb ;


    float tableHeight = 0.725 ;
    addBoxToCollisionModel(rb, 1.3, 0, tableHeight/2.0, 0.8, 0.8, tableHeight );

    rb.getCommander()->setGripperState("r2", false) ;


    PickUp pick(rb, "r2");

    pick.graspClothFromTable();

    camera_helpers::openni::disconnect(pick.camera);
    //resetCollisionModel() ;
    //setServoPowerOff();

    return 0 ;

}
