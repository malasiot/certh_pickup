#include <ros/ros.h>
#include <robot_helpers/robot.hpp>
#include <robot_helpers/util.hpp>
#include <robot_helpers/geometry.hpp>
#include <camera_helpers/openni_capture.hpp>
#include <certh_pickup/PickUp.h>

#include <cvx/util/imgproc/rgbd.hpp>

#include "grasp/grasp_finder.hpp"

using namespace std ;
using namespace robot_helpers ;
using namespace camera_helpers ;
using namespace Eigen ;
using namespace cvx::util ;

class PickupService {
public:
    PickupService(const string &arm, const string &table, ros::NodeHandle &nh): rb_(arm), arm_(arm), table_(table),
        camera_(arm == "r1" ? "xtion1" : "xtion2"),
        grabber_(camera_) {
        grabber_.connect();
        server_ = nh.advertiseService("pickup", &PickupService::run, this);
    }

    bool run(certh_pickup::PickUp::Request &req,  certh_pickup::PickUp::Response &res) {
        ros::Duration wait = ( req.time_out > 0 ) ? ros::Duration(req.time_out) : ros::Duration(100) ;

        if ( graspClothFromTable(wait) ) {
            res.status = 0 ;
            ROS_INFO( "PICKUP SERVICE: Grasping Success" );
        }
        else {
            res.status = 2 ;
            ROS_INFO( "PICKUP SERVICE: No cloth found on table" );
        }

        return true ;
    }

    bool graspClothFromTable(const ros::Duration &d) ;
    bool findGraspCandidates(vector<GraspCandidate> &cand, cv::Mat &depth, image_geometry::PinholeCameraModel &cam) ;
    bool moveXtionAboveCloth() ;

    // get the table plan as seen from the camera and the associated image mask (e.g. enclosed between corners)
    void getTablePlane(const image_geometry::PinholeCameraModel &cam, Vector4d &plane, cv::Mat &mask) ;

    Vector3d retargetSensor(const cv::Mat &dmap, const vector<cv::Point> &hull, const image_geometry::PinholeCameraModel &cm, double Zd) ;
    bool graspTargetCandidate(const vector<GraspCandidate> &gsp, const cv::Mat &depth, const image_geometry::PinholeCameraModel &cm ) ;


private:

    GraspFinder gfinder_ ;
    ros::ServiceServer server_ ;
    string table_, arm_, camera_ ;
    RobotArm rb_ ;
    OpenNICaptureRGBD grabber_ ;
};


static cv::Mat makeMaskFromCorners(const cv::Size &sz, const cv::Point2d &p1, const cv::Point2d &p2, const cv::Point2d &p3, const cv::Point2d &p4) {
    cv::Mat mask = cv::Mat::zeros(sz, CV_8UC1) ;

    vector<vector<cv::Point>> pts(1) ;
    pts[0].push_back(cv::Point(p1.x, p1.y)) ;
    pts[0].push_back(cv::Point(p2.x, p2.y)) ;
    pts[0].push_back(cv::Point(p3.x, p3.y)) ;
    pts[0].push_back(cv::Point(p4.x, p4.y)) ;

    cv::fillPoly(mask, pts, cv::Scalar(255));

    return mask ;
}

void PickupService::getTablePlane(const image_geometry::PinholeCameraModel &cam, Vector4d &plane, cv::Mat &mask) {
    Affine3d cam_frame = getTransform(camera_ + "_rgb_optical_frame", "base_link") ;

//    Affine3d table_frame = getTransform("base_link", table_ + "_desk") ;
//    double height = table_frame.translation().z() ;

//    Vector3d c1 = cam_frame * getTransform("base_link", table_ + "_leg_1").translation()  ;
//    Vector3d c2 = cam_frame * getTransform("base_link", table_ + "_leg_2").translation() ;
//    Vector3d c3 = cam_frame * getTransform("base_link", table_ + "_leg_3").translation() ;
//    Vector3d c4 = cam_frame * getTransform("base_link", table_ + "_leg_4").translation() ;

    Affine3d tray_frame = getTransform("base_link", "tray_bottom") ;
    double height = tray_frame.translation().z() ;

    Vector3d c1 = cam_frame * getTransform("base_link", "tray_wall_1").translation();
    Vector3d c2 = cam_frame * getTransform("base_link", "tray_wall_2").translation();
    Vector3d c3 = cam_frame * getTransform("base_link", "tray_wall_3").translation();
    Vector3d c4 = cam_frame * getTransform("base_link", "tray_wall_4").translation();

    cv::Point2d p1 = cam.project3dToPixel(cv::Point3d(c1.x(), c1.y(), c1.z())) ;
    cv::Point2d p2 = cam.project3dToPixel(cv::Point3d(c2.x(), c2.y(), c2.z())) ;
    cv::Point2d p3 = cam.project3dToPixel(cv::Point3d(c3.x(), c3.y(), c3.z())) ;
    cv::Point2d p4 = cam.project3dToPixel(cv::Point3d(c4.x(), c4.y(), c4.z())) ;

    mask = makeMaskFromCorners(cam.fullResolution(), p1, p2, p3, p4) ;

    // transform table plane equation to camera frame

    plane = Vector4d(0, 0, 1, -height) ;
    plane = cam_frame.matrix().adjoint() * plane ;

    double s = plane.segment(0, 3).norm() ;
    plane /= s ;

//    cv::imwrite("/tmp/mask.png", mask) ;
}

Vector3d PickupService::retargetSensor(const cv::Mat &dmap, const vector<cv::Point> &hull, const image_geometry::PinholeCameraModel &cm, double Zd)
{
    double minZ, maxZ ;

    cv::Mat clr_ ;
    cv::minMaxLoc(dmap, &minZ, &maxZ) ;

    // compute the minimum distance of the sensor from the table

    minZ = maxZ/1000.0 + 0.7 ;

    // find the center of the object and re-target the sensor

    cv::Point2f center ;
    float rad ;
    cv::minEnclosingCircle(hull, center, rad) ;

    double zS = Zd * rad / std::min(dmap.cols, dmap.rows) ;

//    cv::circle(clr_, center, rad, cv::Scalar(255, 0, 255)) ;

    zS = std::max(minZ, zS) ;

    zS = 0.7 + maxZ/1000 ;

    cv::Point3d p = cm.projectPixelTo3dRay(cv::Point2d(center.x, center.y));

    p.x *= zS ; p.y *= zS ; p.z *= zS ;

    return Vector3d(p.x, p.y, p.z) ;

}

bool PickupService::moveXtionAboveCloth() {
    // move sensor above table

    const double initial_sensor_distance = 1.0 ;

    if ( !moveXtionAboveTray(rb_, initial_sensor_distance) ) return false ;

    // grab image of heap

    image_geometry::PinholeCameraModel cam ;

    cv::Size sz(640, 480);

    cv::Mat rgb = cv::Mat::zeros(sz, CV_8UC3) ;
    cv::Mat depth = cv::Mat::zeros(sz, CV_8UC3) ;
    cv::Mat mask = cv::Mat::zeros(sz, CV_8UC1) ;
    cv::Mat /*rgb, depth, mask,*/ dmap ;
    ros::Time ts ;

    if ( !grabber_.grab(rgb, depth, ts, cam) ) return false ;
    cv::imwrite("/tmp/second_grab.png", rgb);
    cv::imwrite("/tmp/second_grab_depth.png", depth);

    // get table plane and mask
    Vector4d plane ;
    getTablePlane(cam, plane, mask) ;

    cv::imwrite("/tmp/mask1.png", mask) ;

    PinholeCamera pcam(cam.fx(), cam.fy(), cam.cx(), cam.cy(), rgb.size()) ;
    vector<cv::Point> hull ;

    // segment largest blob
    if ( !gfinder_.findObjectHull(pcam, depth, mask, plane, hull, dmap) ) return false ;

    for (uint i=0; i<hull.size(); i++)
    {
        cv::circle(rgb, hull[i], 8, cv::Scalar(255, 0, 0)) ;
    }
    cv::imwrite("/tmp/hull.png", rgb) ;

    // retarget camera to center above targer

    Vector3d t = retargetSensor(dmap, hull, cam, initial_sensor_distance - 0.6) ;

    hull.clear();

    Affine3d cam_frame = getTransform("base_link", camera_ + "_rgb_optical_frame") ;

    Vector3d target = cam_frame * t ;
    double new_dist = target.z() +  0.8 ;

    if ( !rb_.moveXtionIK( Vector3d(target.x(), target.y(), target.z() + 0.8), lookAt(Vector3d(0, 0, -1))) )
        return false ;

    if ( !grabber_.grab(rgb, depth, ts, cam) ) return false ;
    cv::imwrite("/tmp/rgb.png", rgb);

    return true ;

}

bool PickupService::findGraspCandidates(vector<GraspCandidate> &cand, cv::Mat &depth, image_geometry::PinholeCameraModel &cam) {


    cv::Mat rgb, mask ;
    ros::Time ts ;

    if ( !grabber_.grab(rgb, depth, ts, cam) ) return false ;

    Vector4d plane ;
    getTablePlane(cam, plane, mask) ;
    cv::imwrite("/tmp/mask2.png", mask) ;

    cv::imwrite("/tmp/pickup_depthCloth.png", depth) ;
    cv::imwrite("/tmp/pickup_rgbCloth.png", rgb) ;

    PinholeCamera pcam(cam.fx(), cam.fy(), cam.cx(), cam.cy(), rgb.size()) ;
    return gfinder_.find(rgb, depth, mask, pcam, plane, cand) ;
}

bool PickupService::graspTargetCandidate( const vector<GraspCandidate> &cand, const cv::Mat &depth, const image_geometry::PinholeCameraModel &cm ){

    bool first_attempt = true ;
    vector<geometry_msgs::Pose> poses;
    GraspCandidate prev_gsp ;

    rb_.setRobotSpeed(0.8);

    rb_.openGripper() ;

    for ( const GraspCandidate &gsp: cand ) {
        if ( !first_attempt ) {
            double dist = sqrt( (gsp.x_ - prev_gsp.x_) * (gsp.x_ - prev_gsp.x_) + (gsp.y_ - prev_gsp.y_) * (gsp.y_ - prev_gsp.y_) ) ;
            if ( dist < 20 ) continue ;
        }
        else first_attempt = false ;

        prev_gsp = gsp ;

        poses.clear();

        // depth to PC
        ushort zval ;
        if  (!sampleNearestNonZeroDepth(depth, gsp.x_, gsp.y_, zval) )  continue ;
        cv::Point3d val = cm.projectPixelTo3dRay(cv::Point2d(gsp.x_, gsp.y_));

        cout << "Grasp point selected in pixels: (" << gsp.x_ << ", " << gsp.y_ << ")" << endl;

        cv::Mat rgb_cloth = cv::imread("/tmp/pickup_rgbCloth.png");
        cv::circle(rgb_cloth, cv::Point(gsp.x_, gsp.y_), 8, cv::Scalar(255, 255, 255)) ;
        cv::imwrite("/tmp/rgb.png", rgb_cloth);


        val *= zval/1000.0 ;

        Eigen::Vector3d p(val.x, val.y, val.z) ;

        Affine3d cam_frame = getTransform("base_link", camera_ + "_rgb_optical_frame") ;

        // target point in robot coordinates

        Eigen::Vector3d target = cam_frame * p ;
        std::cout << target << std::endl;

        const float pregrasp_offset = 0.10, grasp_offset = 0.02 ;
//        const float pregrasp_offset = 0.10, grasp_offset = 0.01 ;

        target.z() += pregrasp_offset ;

        // move gripper to pre-grasp position

        if ( !rb_.moveTipIK(target, lookAt(Vector3d(0, 0, -1), gsp.alpha_)) ) continue ;

        double delta = pregrasp_offset + grasp_offset ;

        rb_.setRobotSpeed(0.1);

        if ( !rb_.moveTipCartesian(Vector3d(0, 0, delta)) ) continue ;

        rb_.closeGripper() ;

//        rb_.moveHome() ;

        RobotArm::Plan plan ;
        Quaterniond q;
        q = robot_helpers::lookAt(Vector3d(0, 0, -1), 0);


        if ( !rb_.planTipIK(Vector3d(target.x(), target.y(), 1.40), q, plan) )
        {
            cerr << "can't plan to location:" << endl;
        }else
        {
            if ( rb_.execute(plan) )
            {
                cout << "tip at: " << rb_.getTipPose().translation().adjoint() <<endl  ;
            }
        }

        ros::Duration(1).sleep();

        if ( !rb_.planTipIK(Vector3d(-0.28, -0.84, 1.40), q, plan) )
        {
            cerr << "can't plan to location:" << endl;
        }else
        {
            if ( rb_.execute(plan) )
            {
                cout << "tip at: " << rb_.getTipPose().translation().adjoint() <<endl  ;
            }
        }

        rb_.openGripper() ;


        break ;


    }

}

bool PickupService::graspClothFromTable(const ros::Duration &dur) {

    ros::Time ts0 = ros::Time::now() + dur ;

    // position sensor to center of the object on heap
    if ( !moveXtionAboveCloth() ) return false;

    // find grasp candidates
    vector<GraspCandidate> gsp ;
    cv::Mat depth ;
    image_geometry::PinholeCameraModel cam ;

    if ( !findGraspCandidates( gsp, depth, cam ) ) return false ;

    if( !graspTargetCandidate( gsp, depth, cam ) ) return false ;

    return true ;
}



int main(int argc, char **argv)
{
    if ( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug) ) {
        ros::console::notifyLoggerLevelsChanged();
    }

    ros::init(argc, argv, "pick_cloth_from_table_service");

    ros::AsyncSpinner spinner(4) ;
    spinner.start() ;

    string table = "t1", arm = "r2" ;

    ros::NodeHandle nh("~") ;
    nh.getParam("table", table) ;
    nh.getParam("arm", arm) ;

    PickupService service(arm, table, nh) ;


//    while(1);
    ros::waitForShutdown();
    spinner.stop();

}
