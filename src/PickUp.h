#ifndef PICKUP_H__
#define PICKUP_H__

#include <robot_helpers/Geometry.h>
#include <robot_helpers/Robot2.h>

#include <camera_helpers/OpenNICapture.h>

#include <certh_libs/ObjectOnPlaneDetector.h>
#include <certh_libs/RidgeDetector.h>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

#include <Eigen/Geometry>
#include <cv.h>

#define MAX_SPEED  0.2
#define MID_SPEED  0.1
#define MIN_SPEED  0.05

class PickUp {

public:

    PickUp(robot_helpers::Robot2 &rb, const std::string &arm, const std::string &table = "") ;
    virtual ~PickUp() ;

    std::string camera, table;

    bool moveAboveTable( ) ;
    bool moveXtionAboveCloth() ;
    bool graspClothFromTable(const ros::Duration &dur = ros::Duration(100)) ;
    void shakeIt() ;
    bool clothFound_ ;
    robot_helpers::Robot2 &robot ;
    
    bool findGraspCandidates(std::vector<certh_libs::RidgeDetector::GraspCandidate> &gsp, std::vector<certh_libs::RidgeDetector::GraspCandidate> &gspref, cv::Mat  &depth, image_geometry::PinholeCameraModel &cm ) ;
    bool graspTargetCandidate( std::vector<certh_libs::RidgeDetector::GraspCandidate> &gsp, cv::Mat &depth, const image_geometry::PinholeCameraModel &cm ) ;
    bool isGraspingSucceeded() ;
    bool isXtionAvailable();


private:


    Eigen::Vector3d parkingPose ;

    std::string armName ;

    cv::Mat prevDepth ;
    certh_libs::RidgeDetector::GraspCandidate prevGsp;
    bool firstAttempt  ;
    double defaultDist ;
    double defaultTableHeight ;
    bool xtion_available;


    Eigen::Vector3d retargetSensor(const cv::Mat dmap, const std::vector<cv::Point> &hull, const image_geometry::PinholeCameraModel &cm, double Zd) ;
    Eigen::Affine3d getSensorPose(const std::string &camera) ;
    bool clothFound(cv::Mat dmap) ;

    geometry_msgs::Quaternion findAngle(float theta, Eigen::Matrix4d calib);
    bool findRotMat(float theta, Eigen::Matrix4d calib);

    void setupTable(const std::string &table) ;

    bool findClothHull( cv::Mat  &depth, cv::Mat &dmap, cv::Mat &mask, std::vector<cv::Point> &hull, const image_geometry::PinholeCameraModel &cm  ) ;
    bool isGraspingSucceeded2();

    void visualizeGraspCandidates(const cv::Mat &depth, const image_geometry::PinholeCameraModel &cm, const std::vector<certh_libs::RidgeDetector::GraspCandidate> &gsp);
    bool isGspValid(certh_libs::RidgeDetector::GraspCandidate gsp, unsigned int bb);

    cv::Mat doSegmentation(cv::Mat &clr, cv::Mat &tablemask) ;

    ros::NodeHandle nh_ ;
    image_transport::ImageTransport it_ ;
    image_transport::Publisher image_pub_ ;
    ros::Publisher vis_pub_ ;


};

#endif

