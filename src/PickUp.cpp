#include "PickUp.h"
#include <robot_helpers/Utils2.h>
#include <moveit/robot_state/conversions.h>

#include <camera_helpers/OpenNIServiceClient.h>
#include <camera_helpers/Util.h>
#include <highgui.h>
#include <certh_libs/cvHelpers.h>
#include <certh_libs/Segmentation.h>

#define DEBUG


using namespace certh_libs ;
using namespace camera_helpers ;
using namespace robot_helpers ;
using namespace std ;
using namespace Eigen ;

PickUp::PickUp(Robot2 &rb, const string &arm, const string &table_): nh_("~"), robot(rb), table(table_), it_(nh_) {

    image_pub_ = it_.advertise("image", 1) ;
    vis_pub_ = nh_.advertise<visualization_msgs::MarkerArray>( "grasp_points", 0 );

    parkingPose <<  getTransformation(table+"_desk" ).getOrigin().x(), getTransformation(table+"_desk" ).getOrigin().y(), getTransformation(table+"_desk" ).getOrigin().z() ;

    if(table_ == "t2"){
        parkingPose.x() -= 0.1 ;
        parkingPose.z() += 0.58 ;
        armName = "r2" ;
    }
    else{
        parkingPose.x() += 0.1 ;
        parkingPose.z() += 0.58 ;
        armName = "r1" ;
    }

    defaultDist = 1.0 ;
    defaultTableHeight = 0.75 ;
    rb.setCurrentGroup( armName + "_arm" );
    if ( !table_.empty() ) setupTable(table_) ;

    camera = ( armName == "r1" ) ? "xtion1" : "xtion2" ;
    firstAttempt = true ;
    xtion_available = camera_helpers::openni::connect(camera) ;
    ros::Duration(0.3).sleep() ;
}

PickUp::~PickUp() {
   camera_helpers::openni::disconnect(camera) ;

}

bool PickUp::isXtionAvailable()
{
    return xtion_available;
}


bool PickUp::moveAboveTable()
{
    robot.getCommander()->setRobotSpeed(0.2); ;
    robot.setStartStateToCurrentState();
    clopema_robot::ClopemaRobotCommander::Plan traj;
    if ( robot.planXtionToPose(parkingPose,robot_helpers::lookAt(Vector3d(0, 0, -1)),traj) )
    {
        robot.execute(traj) ;
        return true ;
    }
}

void PickUp::setupTable(const string &table)
{

    Affine3d tr = robot.getTransform(table + "_desk") ;
    tr = tr.inverse() ;
    parkingPose = tr.translation() ;
    parkingPose.z() += 1 ;
    defaultTableHeight = tr.translation().z() ;

}

Vector3d PickUp::retargetSensor(const cv::Mat dmap, const vector<cv::Point> &hull, const image_geometry::PinholeCameraModel &cm, double Zd)
{
    double minZ, maxZ ;

    cv::Mat clr_ ;
    cv::minMaxLoc(dmap, &minZ, &maxZ) ;

    // compute the minimum distance of the sensor from the table

    minZ = maxZ/1000.0 + 0.7 ;

    // find the center of the object and re-target the sensor

    cv::Point2f center ;
    float rad ;
    cv::minEnclosingCircle(hull, center, rad) ;

    double zS = Zd * rad / std::min(dmap.cols, dmap.rows) ;

    cv::circle(clr_, center, rad, cv::Scalar(255, 0, 255)) ;

    zS = std::max(minZ, zS) ;

    zS = 0.7 + maxZ/1000 ;

    cv::Point3d p = cm.projectPixelTo3dRay(cv::Point2d(center.x, center.y));

    p.x *= zS ; p.y *= zS ; p.z *= zS ;

    return Vector3d(p.x, p.y, p.z) ;

}



bool PickUp::findClothHull( cv::Mat  &depth, cv::Mat &dmap, cv::Mat &mask, vector<cv::Point> &hull, const image_geometry::PinholeCameraModel &cm  ){

    ObjectOnPlaneDetector objDet(depth, cm.fx(), cm.fy(), cm.cx(), cm.cy()) ;

    Eigen::Vector3d n ;
    double d ;

    if ( !objDet.findPlane(n, d) ) return false ;

    mask = objDet.findObjectMask(n, d, 0.01, dmap, hull) ;

    return true ;

}

cv::Mat PickUp::doSegmentation(cv::Mat &clr, cv::Mat &tablemask){

    certh_libs::Segmentation srv ;
    srv.request.rgb = *camera_helpers::cvImageToMsg(clr) ;
    certh_libs::createMaskFromDepth(tablemask,1500) ;
    srv.request.mask = *camera_helpers::cvImageToMsg(tablemask) ;
    ros::service::waitForService("/texture_segmentation") ;

    if(!ros::service::call("/texture_segmentation", srv)) {
        ROS_ERROR_STREAM("cant call texture_segmentation");

    }

    boost::shared_ptr<sensor_msgs::Image> MASKptr(boost::make_shared<sensor_msgs::Image>(srv.response.mask));
    cv::Mat clothOnTableMask = camera_helpers::msgToCvImage(MASKptr) ;

    cv::imwrite("/tmp/clothMask.png", clothOnTableMask);

    return clothOnTableMask ;

}

Affine3d PickUp::getSensorPose(const string &camera)
{
    return robot.getTransform(camera + "_rgb_optical_frame") ;
}

bool PickUp::clothFound(cv::Mat dmap){

    double min, max;
    cv::minMaxLoc(dmap, &min, &max) ;
    ROS_DEBUG("PICKUP NODE: max value in cloth hull = %f", max) ;
    if((max-min) > 10) return true ;
    else return false ;

}

bool PickUp::moveXtionAboveCloth(){

    if ( !moveAboveTable() ) return false ;

    cv::Mat rgb, depth;

    ros::Time ts ;
    image_geometry::PinholeCameraModel cm ;

    ros::Duration(1).sleep() ;

    if( !openni::grab(camera, rgb, depth, ts, cm) ) {
       ROS_WARN("PICKUP NODE: Can't grab image above table!") ;
       return false ;
    }


    cv::imwrite("/tmp/pickup_rgbTable.png", rgb) ;
    cv::imwrite("/tmp/pickup_depthTable.png", depth) ;

    prevDepth = depth ;

    cv::Mat dmap, mask ;
    vector<cv::Point> hull ;
    bool hullFound = findClothHull(depth, dmap, mask, hull, cm);

    cv::imwrite("/tmp/pickup_clothHull.png", dmap) ;

    clothFound_ = clothFound(dmap) ;

    ROS_DEBUG("PICKUP NODE: Cloth found = %d", clothFound_) ;

    if ( !clothFound_ || !hullFound ) {
        ROS_INFO("PICKUP NODE: Can't pickup") ;
        return false ;
    }

    Affine3d frame = getSensorPose(camera) ;

    Vector3d t = retargetSensor(dmap, hull, cm, defaultDist - defaultTableHeight) ;

    Vector3d target = frame.inverse() * t ;

    double newDist = target.z() +  0.7 ;
    robot.setStartStateToCurrentState();
    clopema_robot::ClopemaRobotCommander::Plan traj;
    if ( robot.planXtionToPose( Vector3d(target.x(), target.y(), newDist ), robot_helpers::lookAt(Vector3d(0, 0, -1)), traj) )
    {
        robot.execute(traj) ;
        return true ;
    }

    return false ;


}

void PickUp::visualizeGraspCandidates(const cv::Mat &depth, const image_geometry::PinholeCameraModel &cm,
                                     const std::vector<certh_libs::RidgeDetector::GraspCandidate> &gsp)
{
    visualization_msgs::MarkerArray markers ;


    for(int i=0 ; i<gsp.size() ; i++ )
    {
        visualization_msgs::Marker marker ;

        const RidgeDetector::GraspCandidate &cnd = gsp[i] ;

        ushort z ;

        if ( !certh_libs::sampleNearestNonZeroDepth(depth, cnd.x, cnd.y, z) ) continue ;

        cv::Point3d p = cm.projectPixelTo3dRay(cv::Point2d(cnd.x, cnd.y)) ;

        p *= z/1000.0 ;

        marker.header.frame_id = "xtion2_rgb_optical_frame";
        marker.header.stamp = ros::Time();
        marker.ns = "grasp_candidates";
        marker.id = i;
        marker.type = visualization_msgs::Marker::SPHERE;
        marker.action = visualization_msgs::Marker::ADD;
        marker.pose.position.x = p.x;
        marker.pose.position.y = p.y;
        marker.pose.position.z = p.z;
        marker.pose.orientation.x = 0.0;
        marker.pose.orientation.y = 0.0;
        marker.pose.orientation.z = 0.0;
        marker.pose.orientation.w = 1.0;
        marker.scale.x = 0.01;
        marker.scale.y = 0.01;
        marker.scale.z = 0.01;
        marker.color.a = 1.0;

        marker.color.r = 0.0;
        marker.color.g = 1.0;
        marker.color.b = 0.0;

        marker.lifetime = ros::Duration(-1) ;

        markers.markers.push_back(marker) ;

    }

    vis_pub_.publish(markers) ;

}

bool PickUp::findGraspCandidates( vector<RidgeDetector::GraspCandidate> &gsp, vector<RidgeDetector::GraspCandidate> &gspref, cv::Mat  &depth, image_geometry::PinholeCameraModel &cm  ){

    cv::Mat rgb, seg_mask, table_mask;

    ros::Time ts ;


    if(!openni::grab(camera, rgb, depth, ts, cm) ){
       ROS_WARN("PICKUP NODE: Can't grab image above Cloth!") ;
       return false ;
    }

    cv::imwrite("/tmp/pickup_depthCloth.png", depth) ;
    cv::imwrite("/tmp/pickup_rgbCloth.png", rgb) ;

    DepthFilter filter ;
    filter.push(depth, rgb) ;

    depth = filter.filter(DEPTH_FILTER_JOINT_BILATERAL_FAST) ;


    cv::Mat dmap, mask ;
    vector<cv::Point> hull ;

    if ( !findClothHull(depth, dmap, mask, hull, cm)){
        ROS_WARN("PICKUP NODE: cant find cloth hull!") ;
        return false ;
    }


    RidgeDetector rdg ;

    rdg.detect(dmap, gsp) ;

    rdg.refineCandidatesByBlob(rgb, dmap, mask, gsp, gspref) ;

    cv::Mat rgbtemp = rgb.clone() ;
    rdg.draw(rgbtemp, gsp) ;

    cv::imwrite("/tmp/pickup_gsp.png", rgbtemp) ;

    rdg.draw(rgb, gspref) ;

    cv::imwrite("/tmp/pickup_gspref.png", rgb) ;

    sensor_msgs::ImagePtr msg = cvImageToMsg(rgb) ;


    image_pub_.publish(msg) ;

    visualizeGraspCandidates(depth, cm, gsp) ;

    return true ;
}

geometry_msgs::Quaternion PickUp::findAngle(float theta, Eigen::Matrix4d calib){

    Eigen::Vector4d norm ;
    norm << cos(theta), sin(theta), 0, 0 ;

    Eigen::Vector4d targetN ;
    targetN = calib * norm.normalized() ;

    Eigen::Matrix3d rotMat;
    Eigen::Vector3d Y(targetN.x(), targetN.y(), targetN.z());
    Eigen::Vector3d X;
    Eigen::Vector3d Z(0, 0, -1);

    X = Y.cross(Z);

    rotMat(0, 0)=X.x();
    rotMat(1, 0)=X.y();
    rotMat(2, 0)=X.z();

    rotMat(0, 1)=Y.x();
    rotMat(1, 1)=Y.y();
    rotMat(2, 1)=Y.z();

    rotMat(0, 2)=Z.x();
    rotMat(1, 2)=Z.y();
    rotMat(2, 2)=Z.z();

    //vect << X.x() , X.y() , X.z() ;

    return rotationMatrix3ToQuaternion(rotMat);
}

 bool PickUp::findRotMat(float theta, Eigen::Matrix4d calib){

    Eigen::Vector4d norm ;
    norm << cos(theta), sin(theta), 0, 0 ;

    Eigen::Vector4d targetN ;
    targetN = calib * norm.normalized() ;

    Eigen::Matrix3d rotMat;
    Eigen::Vector3d Y(targetN.x(), targetN.y(), targetN.z());
    Eigen::Vector3d X;
    Eigen::Vector3d Z(0, 0, -1);

    X = Y.cross(Z);

    rotMat(0, 0)=X.x();
    rotMat(1, 0)=X.y();
    rotMat(2, 0)=X.z();

    rotMat(0, 1)=Y.x();
    rotMat(1, 1)=Y.y();
    rotMat(2, 1)=Y.z();

    rotMat(0, 2)=Z.x();
    rotMat(1, 2)=Z.y();
    rotMat(2, 2)=Z.z();

    return true ;
}

 bool PickUp::isGraspingSucceeded(){


    for(unsigned int i=0 ; i<5 ; i++){
        if(moveAboveTable())
            break ;
        else{
            robot.setGripperState(armName, true, true) ;
            return false ;
        }
    }
    if( robot.isGripperHolding(armName) )
        return true ;
    return false ;

 }

//bool PickUp::isGraspingSucceeded(float threshold){

//    for(unsigned int i=0 ; i<5 ; i++){
//        if(moveAboveTable())
//            break ;
//        else{
//            robot.setGripperState(armName, true, true) ;
//            return false ;
//        }
//    }


//    cv::Mat rgb, depth;

//    ros::Time ts ;
//    image_geometry::PinholeCameraModel cm ;

//    if(!openni::grab(camera, rgb, depth,  ts, cm)){
//        ROS_WARN("PICKUP NODE: Cant grab image for grasping confirmation ") ;
//        return false ;
//    }

//    vector <float> diff ;

//    float oldCap , newCap;

//    for ( int i = 0; i < depth.rows ; i++){
//        for ( int j = 0; j < depth.cols ; j++){

//            oldCap = (float)prevDepth.at<unsigned short>(i,j) ;
//            newCap = (float)depth.at<unsigned short>(i,j) ;

//            if( (oldCap || newCap) == 0 )
//                continue;

//            diff.push_back ( abs(oldCap - newCap) ) ;

//        }
//    }

//    float sum = 0, meanDepthDiff = 0;

//    for ( int i = 0; i < diff.size() ; i++)
//        sum += diff[i] ;



//    meanDepthDiff = (float)sum / (float)diff.size();

//    ROS_DEBUG( "PICKUP NODE: Mean Error per pixel = %f", meanDepthDiff) ;

//    if (meanDepthDiff > threshold)
//        return true ;

//    robot.setGripperState(armName, true, true);
//    return false ;

//}


bool PickUp::isGraspingSucceeded2(){

   for(unsigned int i=0 ; i<5 ; i++){
        if(moveAboveTable())
            break ;
        else{
            robot.setGripperState(armName, true, true) ;
            return false ;
        }
    }


    cv::Mat rgb, depth;
    unsigned int count = 0 ;
    ros::Time ts ;
    image_geometry::PinholeCameraModel cm ;

    if(!openni::grab(camera, rgb, depth,  ts, cm)){
        cout<<" Cant grab image!!  " << endl ;
        return false ;
    }

    for(unsigned int i=200 ; i < 480 ; i++){
        for(unsigned int j=160 ; j < 480 ; j++){

            if(depth.at<unsigned short> (i,j) > 200 && depth.at<unsigned short> (i,j) < 600){
                count++ ;
                //cout<< "count =" << count << "i = "<< i << "j = " << j << endl;
//                if(count > 1000)
//                   return true ;
            }
        }

    }

    return false ;

}


bool PickUp::isGspValid(RidgeDetector::GraspCandidate gsp, unsigned int rad){

    int dist = (int) sqrt( abs(gsp.x - prevGsp.x) * abs(gsp.x - prevGsp.x) - abs(gsp.y - prevGsp.y) * abs(gsp.y - prevGsp.y) ) ;

    if(dist < rad)
        return false ;
    return true ;
}



bool PickUp::graspTargetCandidate( vector<RidgeDetector::GraspCandidate> &gspref, cv::Mat &depth, const image_geometry::PinholeCameraModel &cm ){

    bool grasp = false;
    vector <geometry_msgs::Pose> poses;


    robot.setGripperState(armName, true, true);

    for (unsigned int i = 0 ; i < gspref.size() ; i++ ){

        if(!firstAttempt){
            if(!isGspValid(gspref[i], 20))
                continue ;
        }

        poses.clear();

        // depth to PC
        unsigned short zval = depth.at<ushort>(gspref[i].y, gspref[i].x) ;
        cv::Point3d val = cm.projectPixelTo3dRay(cv::Point2d(gspref[i].x, gspref[i].y));

        val.x *= zval/1000.0 ;
        val.y *= zval/1000.0 ;
        val.z *= zval/1000.0 ;

        Eigen::Vector3d p(val.x, val.y, val.z) ;

        Eigen::Matrix4d calib = getSensorPose(camera ).matrix() ;
        Eigen::Vector4d tar (p.x(), p.y(), p.z(), 1) ;
        Eigen::Vector4d targetP ;

        targetP = calib.inverse() * tar ;

       // float pregraspOffset= 0.10 , gripperGapOffset = 0.015, graspZOffset = 0.03 ;

        float pregraspOffset= 0.10, graspZOffset = 0.04 ;

        geometry_msgs::Pose pose ;

        pose.orientation = findAngle(gspref[i].alpha, calib) ;
        pose.position.x = targetP.x()  ;
        pose.position.y = targetP.y()   ;
        pose.position.z = targetP.z() + pregraspOffset ;

        clopema_robot::ClopemaRobotCommander::Plan traj1, traj2,  traj ;
        Vector3d pos1, pos2;
        Quaterniond q1, q2 ;
        robot_helpers::RosPoseToEigenVectorQuaternion(pos1, q1, pose) ;

//        pos1.x()-= 0.07 ;
//        pos1.y()-= 0.035 ;
//        pos1.z()-= 0.03 ;

        // calculating direction for gripper gap offset
        Eigen::Vector3d pregrasp(1,0,0) ;
        Eigen::Vector3d preRot, prePos ;
        preRot =  q1._transformVector(pregrasp) ;
        prePos = pos1 ;
       // prePos.x() -= 0.01 , prePos.y() += 0.015 ;

        pos2 << prePos.x(), prePos.y(), prePos.z() - pregraspOffset - graspZOffset ;
        q2 = q1 ;

        robot.setStartStateToCurrentState();
        if( robot.planGripperToPoseIK(prePos,q1 , traj1, "_gg") ) {

            traj.trajectory_ = traj1.trajectory_ ;

            moveit_msgs::RobotState start_state ;
            start_state.joint_state.name = traj.trajectory_.joint_trajectory.joint_names ;
            start_state.joint_state.position = traj.trajectory_.joint_trajectory.points.back().positions ;
            robot.setStartState(start_state);


            if(planArmCartesian(robot, pos2, q2, traj2, robot.getArmName(), "_gg")) {

                robot.getCommander()->setRobotSpeed(MAX_SPEED);
                robot.execute(traj1) ;
                robot.getCommander()->setRobotSpeed(MIN_SPEED);
                robot.execute(traj2) ;

                grasp = true;
                prevGsp = gspref[i] ;
                firstAttempt =  false;
                //cout << "dominant grasping point i = " <<  i << endl;
                break;
            }

        }

        ////robot.moveGripperToPose(armName, pos2, q);

   }

    if(grasp){
        robot.setGripperState( armName, false, true) ;
        return true ;
    }
    else
        return false;
}


//Eigen::Vector3d pregrasp(0,0,1) ;
//Eigen::Vector3d preRot, prePos ;
//preRot =  q._transformVector(pregrasp) ;
//prePos = 0.2 * -preRot + pos ;
//clopema_robot::ClopemaRobotCommander::Plan traj1, traj2, traj ;

//if( robot.planPsToPose(prePos, q, traj1) ){

//    traj.trajectory_ = traj1.trajectory_ ;
//    moveit_msgs::RobotState start_state ;
//    start_state.joint_state.name = traj1.trajectory_.joint_trajectory.joint_names ;
//    start_state.joint_state.position = traj1.trajectory_.joint_trajectory.points.back().positions ;
//    robot.setStartState(start_state);

//    if( planArmCartesian(robot, pos, q, traj2, robot.getArmName(), "_ps") ){

//        robot.execute(traj1) ;
//        robot.execute(traj2) ;


//    }




void PickUp::shakeIt(){

    robot.getCommander()->setRobotSpeed(0.2) ;
    Eigen::Quaterniond q ;
    Eigen::Vector3d pose ;
    vector <Eigen::Affine3d> poses ;
    getArmPose(armName, pose,q) ;

    pose.z()-=0.1 ;
    poses.push_back(eigenVectorQuaternionToAffine(pose, q) );
    pose.z()+=0.1 ;
    poses.push_back(eigenVectorQuaternionToAffine(pose, q) );

    moveArmThrough(robot,poses) ;
    robot.getCommander()->setRobotSpeed(0.1) ;

}

bool PickUp::graspClothFromTable(const ros::Duration &dur){

    bool grasp = false;

    ros::Time ts0 = ros::Time::now() + dur ;

    while (!grasp){

        if(!moveXtionAboveCloth() )
            return false;
        vector<RidgeDetector::GraspCandidate> gsp, gspref ;
        cv::Mat  depth ;
        image_geometry::PinholeCameraModel cm ;

        findGraspCandidates( gsp, gspref, depth, cm ) ;
        if( !graspTargetCandidate( gspref, depth, cm ) )
            return false ;

        grasp = isGraspingSucceeded() ;
        if(!grasp)
            robot.setGripperState(armName, true);
    }

    return true ;

}

